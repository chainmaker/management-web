import create from 'zustand';
import { PluginStateStore, pluginStateStore } from './pluginStateStore';

export type IdeStore = PluginStateStore;

const useAppStore = create<IdeStore>((...param) => ({
  ...pluginStateStore(...param),
}));
export default useAppStore;
