/*
 *
 *  Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
 *  SPDX-License-Identifier: Apache-2.0
 *
 */
import { useCallback, useState } from 'react';
import {
  fetchCertList,
  fetchDeleteCert,
  fetchDetailCert,
  fetchDownloadCert,
  fetchGenerateCert,
  fetchImportCert,
  fetchOneGenerateCert,
} from '.';
import {
  ICertItem,
  IDetailCertParam,
  IDownloadCertParam,
  IFetchCertListParam,
  IGenerateCertParam,
  IImportCertParam,
  IDetailCertResponse,
} from './interface';
import { useDispatchMessage } from 'src/utils/hooks';
import { saveBlobAs } from '../../../utils/common';
import { Error, ErrorCode } from 'src/common/interface';
import { Modal } from 'tea-component';

/**
 * 获取证书列表
 */
export function useFetchCertList() {
  const { errorNotification } = useDispatchMessage();
  const [list, setList] = useState<ICertItem[]>([]);
  const [totalCount, setTotalCount] = useState<number>(0);

  const fetch = useCallback(async (params: IFetchCertListParam) => {
    try {
      const result = await fetchCertList(params);
      setList(result.data.Response.GroupList);
      setTotalCount(result.data.Response.TotalCount);
    } catch (err) {
      errorNotification({
        title: '获取证书列表失败',
        description: (err as Error).Message,
      });
    }
  }, []);

  return { list, totalCount, fetch };
}
/**
 * 快速生成证书
 */
export function useFetchOneGenCert() {
  const { errorNotification } = useDispatchMessage();
  const fetchOneGenCert = useCallback(async () => {
    try {
      const data = await fetchOneGenerateCert();
      return data.data.Response.Data.Status;
    } catch (err) {
      errorNotification({
        title: '一键生成证书失败',
        description: (err as Error).Message,
      });
    }
  }, []);

  return { fetchOneGenCert };
}
/**
 * 查看证书
 */
export function useFetchCertDetail() {
  const { errorNotification } = useDispatchMessage();
  const [privateKey, setPrivateKey] = useState<string>('');
  const [publicKey, setPublicKey] = useState<string>('');
  const [certDetail, setCertDetail] = useState<{
    SignCertDetail?: string;
    SignKeyDetail?: string;
    TlsCertDetail?: string;
    TlsKeyDetail?: string;
  }>({});
  const fetch = useCallback(async (params: IDetailCertParam) => {
    try {
      const result = await fetchDetailCert(params);
      const data: IDetailCertResponse = result.data.Response.Data;
      setPublicKey(data.PublicKey);
      setPrivateKey(data.PrivateKey);
      setCertDetail(data);
      // data.CertDetail
    } catch (err) {
      errorNotification({
        title: '获取证书失败',
        description: (err as Error).Message,
      });
    }
  }, []);

  return {
    certDetail,
    privateKey,
    publicKey,
    fetch,
  };
}

/**
 * 删除证书
 */
export function useFetchRemoveCertDetail() {
  const { errorNotification, successNotification } = useDispatchMessage();
  const fetchRemoveCert = useCallback(async (params: IDetailCertParam, isPublic?: boolean) => {
    const yes = await Modal.confirm({
      message: '删除确认',
      description: isPublic ? '请确定是否删除该账户？' : '请确定是否删除该证书？',
      okText: '删除',
      cancelText: '取消',
    });
    if (!yes) {
      return;
    }
    try {
      const result = await fetchDeleteCert(params);
      if (result.status === 200) {
        successNotification({
          description: '删除证书成功',
        });
      }
    } catch (err) {
      errorNotification({
        title: '删除证书失败',
        description: (err as Error).Message,
      });
    }
  }, []);

  return {
    fetchRemoveCert,
  };
}

/**
 * 下载证书
 */
export function useFetchDownloadCert() {
  const { errorNotification } = useDispatchMessage();
  const fetch = useCallback(async (params: IDownloadCertParam) => {
    try {
      const result = await fetchDownloadCert(params);
      const { data: blob, headers } = result;
      saveBlobAs(headers, blob);
    } catch (err) {
      errorNotification({
        title: '下载证书失败',
        description: (err as Error).Message,
      });
    }
  }, []);
  return {
    fetch,
  };
}

/**
 * 申请证书
 */
export function useFetchGenerateCert() {
  const { errorNotification, successNotification } = useDispatchMessage();
  const [isSuccess, setIsSuccess] = useState<boolean>(false);
  const fetch = useCallback(async (params: IGenerateCertParam) => {
    try {
      await fetchGenerateCert(params);
      setIsSuccess(true);
      successNotification({
        description: '申请证书成功',
      });
    } catch (err) {
      errorNotification({
        title: '申请证书失败',
        description: (err as Error).Message,
      });
    }
  }, []);
  return {
    isSuccess,
    fetch,
  };
}

/**
 * 导入证书
 */
export function useFetchImportCert() {
  const { errorNotification, successNotification } = useDispatchMessage();
  const fetch = useCallback(async (params: IImportCertParam) => {
    try {
      await fetchImportCert(params);
      successNotification({
        description: '导入证书成功',
      });
    } catch (err) {
      let errMessage = '导入证书失败';
      switch ((err as Error)?.Code) {
        case ErrorCode.AccountExisted:
          errMessage = '该账户已存在，请检查后重新输入。';
          break;
        case ErrorCode.AccountKeyMatch:
          errMessage = '公私钥对不匹配，请检查后重新上传。';
          break;
        case ErrorCode.AlgorithmMatch:
          errMessage = '公私钥对与所选密码算法不符，请检查后重新上传。';
          break;
      }
      errorNotification({
        title: errMessage,
        description: (err as Error).Message,
      });
    }
  }, []);

  return {
    fetch,
  };
}
