/*
 *
 *  Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
 *  SPDX-License-Identifier: Apache-2.0
 *
 */
import { PageQuery } from '../../interface';

export interface IFetchLogListParam extends PageQuery {
  ChainId: string;
}
export interface ILogInfo {
  Id: number;
  Log: string;
  ChainId: string;
  LogTime: number;
  LogId: string;
  NodeId: string;
}

export interface IPullParam {
  ChainId: string;
}

export interface IDownloadLogParam {
  Id: number;
}

export interface IReportLogParam {
  Id: number;
}

export interface IAutoReportParam {
  ChainId: string;
  Automatic: number;
}
