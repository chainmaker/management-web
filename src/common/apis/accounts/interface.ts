/*
 *
 *  Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
 *  SPDX-License-Identifier: Apache-2.0
 *
 */
import { PageQuery } from '../../interface';

export interface IResponseId {
  ResponseId: string;
}

export interface IListResponse<T> {
  ResponseId: string;
  TotalCount: number;
  GroupList: T[];
}

export interface IUserItem {
  Id: number;
  UserName: string;
  Name: string;
  Status: 0 | 1;
  CreateTime: number;
}

export interface IFetchUserListParam extends PageQuery {
  UserId: number;
}

export interface ICreateParam {
  UserName: string;
  Name: string;
  Password: string;
}

export interface IModifyPasswordParam {
  UserId: number;
  Password: string;
  OldPassword: string;
}

export interface IAccountStateParam {
  UserId: number;
}
