/*
 *
 *  Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
 *  SPDX-License-Identifier: Apache-2.0
 *
 */
import React, { useCallback, useContext, useEffect, useState } from 'react';
import { PagingQuery, Table, TableColumn } from 'tea-component';
import { Link, useRouteMatch } from 'react-router-dom';
import { formatDate } from 'src/utils/date';
import { IChainBrowserBlockItem } from 'src/common/apis/chains/interface';
import { useFetchBlockList } from 'src/common/apis/chains/hooks';
import { PAGE_SIZE_OPTIONS, splitUrl } from 'src/utils/common';
import { ChainDetailContext } from '../chain-detail';
const { pageable, autotip } = Table.addons;

export function BlockTable() {
  const chainMode = splitUrl(location.search).get('chainMode');
  const { url } = useRouteMatch();
  const { chainId } = useContext(ChainDetailContext);
  const { list, totalCount, fetch } = useFetchBlockList();
  const [pageQuery, setPageQuery] = useState<Required<PagingQuery>>({ pageSize: 10, pageIndex: 1 });
  const defaultColumn: TableColumn<IChainBrowserBlockItem>[] = [
    {
      key: 'BlockHeight',
      header: '区块高度',
      width: 100,
      // eslint-disable-next-line react/display-name
      render: (record) => {
        const { BlockHeight } = record;
        return <>{BlockHeight}</>;
      },
    },
    {
      key: 'BlockHash',
      header: '区块哈希',
      // eslint-disable-next-line react/display-name
      render: (record) => {
        const { BlockHash, Id } = record;
        return <Link to={`${url}/details?type=block&Id=${Id}&chainMode=${chainMode}`}>{BlockHash}</Link>;
      },
    },
    {
      key: 'TxNum',
      header: '交易数',
      width: 100,
    },
    {
      key: 'NodeName',
      header: '出块节点',
    },
    {
      key: 'Timestamp',
      header: '区块生成时间',
      width: 150,
      // eslint-disable-next-line react/display-name
      render: (record) => {
        const { Timestamp } = record;
        return <>{formatDate(Timestamp)}</>;
      },
    },
  ];

  const fetchList = useCallback(() => {
    if (!chainId) return;
    fetch({
      ChainId: chainId,
      PageNum: pageQuery.pageIndex - 1,
      PageSize: pageQuery.pageSize,
    });
  }, [fetch, pageQuery]);

  useEffect(() => {
    fetchList();
  }, [fetchList]);

  return (
    <>
      <Table
        recordKey="Id"
        records={list}
        columns={defaultColumn}
        addons={[
          pageable({
            recordCount: totalCount,
            pageIndex: pageQuery.pageIndex,
            pageSize: pageQuery.pageSize,
            pageSizeOptions: PAGE_SIZE_OPTIONS,
            onPagingChange: ({ pageIndex, pageSize }) =>
              setPageQuery({ pageIndex: pageIndex ?? 1, pageSize: pageSize ?? 10 }),
          }),
          autotip({
            emptyText: '暂无数据',
          }),
        ]}
      />
    </>
  );
}
