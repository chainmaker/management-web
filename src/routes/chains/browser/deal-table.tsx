/*
 *
 *  Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
 *  SPDX-License-Identifier: Apache-2.0
 *
 */
import React, { useCallback, useContext, useEffect, useState } from 'react';
import { PagingQuery, Table, TableColumn } from 'tea-component';
import { Link, useRouteMatch } from 'react-router-dom';
import { formatDate } from 'src/utils/date';
import { IChainBrowserTxItem } from 'src/common/apis/chains/interface';
import { useFetchDealList } from 'src/common/apis/chains/hooks';
import { PAGE_SIZE_OPTIONS, splitUrl } from 'src/utils/common';
import { ChainDetailContext } from '../chain-detail';
const { pageable, autotip } = Table.addons;

export function DealTable() {
  const { url } = useRouteMatch();
  const { chainId } = useContext(ChainDetailContext);
  const { list, totalCount, fetch } = useFetchDealList();
  const [pageQuery, setPageQuery] = useState<Required<PagingQuery>>({ pageSize: 10, pageIndex: 1 });
  const chainMode = splitUrl(location.search).get('chainMode');
  const isPublic = chainMode === 'public';

  const defaultColumn: TableColumn<IChainBrowserTxItem>[] = [
    {
      key: 'BlockHeight',
      header: '区块高度',
      width: 100,
      // eslint-disable-next-line react/display-name
      render: (record: { BlockHeight: any }) => {
        const { BlockHeight } = record;
        return <>{BlockHeight}</>;
      },
    },
    {
      key: 'TxId',
      header: '交易ID',
      // eslint-disable-next-line react/display-name
      render: (record: { TxId: any; Id: any }) => {
        const { TxId, Id } = record;
        return <Link to={`${url}/details?type=deal&Id=${Id}&chainMode=${chainMode}`}>{TxId}</Link>;
      },
    },
    !isPublic
      ? {
          key: 'OrgName',
          header: '发起组织',
        }
      : {
          key: 'null',
          header: 'null',
        },
    {
      key: 'UserName',
      header: '交易发起用户',
      render: (item: { UserName: string; Addr: string }) => item.Addr || item.UserName,
    },
    {
      key: 'ContractName',
      header: '合约',
    },
    {
      key: 'Timestamp',
      header: '上链时间',
      width: 150,
      // eslint-disable-next-line react/display-name
      render: (record: { Timestamp: any }) => {
        const { Timestamp } = record;
        return <>{formatDate(Timestamp)}</>;
      },
    },
  ].filter((item) => item.key !== 'null');

  const fetchList = useCallback(() => {
    if (!chainId) return;
    fetch({
      ChainId: chainId,
      PageNum: pageQuery.pageIndex - 1,
      PageSize: pageQuery.pageSize,
    });
  }, [fetch, pageQuery]);

  useEffect(() => {
    fetchList();
  }, [fetchList]);

  return (
    <>
      <Table
        recordKey="Id"
        records={list}
        columns={defaultColumn}
        addons={[
          pageable({
            recordCount: totalCount,
            pageIndex: pageQuery.pageIndex,
            pageSize: pageQuery.pageSize,
            pageSizeOptions: PAGE_SIZE_OPTIONS,
            onPagingChange: ({ pageIndex, pageSize }) =>
              setPageQuery({ pageIndex: pageIndex ?? 1, pageSize: pageSize ?? 10 }),
          }),
          autotip({
            emptyText: '暂无数据',
          }),
        ]}
      />
    </>
  );
}
