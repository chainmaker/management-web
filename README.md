# ChainMaker 管理平台


##

fix/v2.3.1 管理台2.3.1版本
version/3.0.0 管理台适配 3.0.0版本的链
lighthouse_v2.3.0 lighthouse版本
feat/v2.3.3 管理台2.3.3版本 适配底链2.3.3
feat/v2.3.4 管理台2.3.4版本 适配底链2.3.4

```mermaid
graph TD
    lighthouse_v2.3.0 --> lighthouse
    fix/v2.3.1 --> version/3.0.0
    version/3.0.0 --> feat/v2.3.3
    feat/v2.3.3 --> feat/v2.3.4
```

## 适配版本一览表

| chainmaker-go | management-backend | management-web |
| :------------ | :----------------- | -------------- |
| v2.3.0        | v2.3.0             | v2.3.0         |

## 开发

### whistle 代理

```shell
# 加载规则配置
$ w2 use
```

访问[http://192.168.1.108:3000](http://192.168.1.108:3000)

```shell
# 安装依赖
$ npm install

# 启动
$ npm start

# 构建打包
$ npm build
```

### 项目结构

```
├── HANDBOOK.md     // 项目配置说明文档
├── LICENSE         // 许可声明文件
├── NOTICE          // 公告信息
├── README.md       // 项目说明文件
├── Dockerfile                      // docker构建配置
├── commitlint.config.js            // commit提交规则配置文件
├── config                          // 环境配置文件目录
│   ├── env.js                      // 读取环境变量
│   ├── getHttpsConfig.js           // http请求配置
│   ├── jest                        // jest相关配置
│   ├── modules.js                  // 模块引用
│   ├── paths.js                    // path映射定义
│   ├── pnpTs.js                    // pnp配置
│   ├── webpack.config.js           // webpack配置
│   └── webpackDevServer.config.js  // webpack服务配置
├── deploy                          // 部署配置
│   └── nginx                       // nginx相关配置
├── package-lock.json               // 项目依赖包版本管理
├── package.json                    // 项目基础配置
├── public                          // 静态资源
│   ├── android-chrome-192x192.png
│   ├── android-chrome-512x512.png
│   ├── apple-touch-icon.png
│   ├── favicon-16x16.png
│   ├── favicon-32x32.png
│   ├── favicon.ico
│   ├── index.html
│   ├── manifest.json
│   ├── robots.txt
│   └── static
├── scripts                          // 启动脚本
├── src                              // 项目开发文件
│   ├── App.css
│   ├── App.test.tsx
│   ├── App.tsx
│   ├── common
│   ├── index.css
│   ├── index.tsx
│   ├── mock-data
│   ├── react-app-env.d.ts
│   ├── reportWebVitals.ts
│   ├── routes
│   ├── routes.tsx
│   ├── setupTests.ts
│   ├── stores
│   ├── typing.d.ts
│   └── utils
└── tsconfig.json                     // ts规则配置文件

```

### 协议

- 所有程序文件头部追加协议声明，参见既存程序文件，如`src/App.tsx`

## 测试

### 测试账户

```
username:admin
password:a123456
```

### 测试预览

访问[http://192.168.1.108](http://192.168.1.108)

## 部署

- 测试`develop分支CI自动部署 `
- 生产`人工锁版`

## 开源

- 项目中采用的第三方资源均为开源

## 发布

- https://jenkins.chainmaker.org.cn/job/ChainMaker_push_docker_hub_management-web/

## 相关文档

- https://tea-design.github.io/component
- https://github.com/nuysoft/Mock/wiki/Getting-Started
